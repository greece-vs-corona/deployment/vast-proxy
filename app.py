import ast
import logging
import os
import re
import http.client
import requests
from flask import Flask, request, redirect, Response, abort
from flask_oidc import OpenIDConnect

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config.update({
    'SECRET_KEY': os.environ.get('SECRET_KEY'),
    'OIDC_CLIENT_SECRETS': '/etc/oidc_client_secrets/client_secrets.json',
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_SCOPES': ['openid', 'email', 'profile', 'read_api'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC_OPENID_REALM': os.environ.get('OPENID_REALM')
})

app.config['OVERWRITE_REDIRECT_URI'] = os.environ.get('OPENID_REALM')

VAST_PUBLIC_URL = os.environ.get('VAST_PUBLIC_URL')

oidc = OpenIDConnect(app)


def remove_login_links(content):
    content = content.decode('utf-8')
    content = re.sub(r'<[Aa]\s*href="login"\s*>[/<>"\w\s]*</[aA]>', '', content)
    content = re.sub(r'<[Aa]\s*href="logout"\s*>[/<>"\w\s]*</[aA]>', '', content)
    content = re.sub(r'<[Aa]\s*href="users\?register"\s*>[/<>"\w\s]*</[aA]>', '', content)
    content = re.sub(r'<[Aa] href="submit">.*</[Aa]>', '', content)
    content = re.sub(r'(?:(<[Ii][Mm][Gg] src="gfx/tab_BB.png".*>)\s*)+', r'\1', content)
    content = content.encode('utf-8')
    return content


def check_allowed_user(access_token):
    """
    checks if a user belongs in the allowed groups based on the provided access token and aborts otherwise

    :param access_token: the access token by oidc provider
    """
    groups_list = get_groups(access_token)

    allowed_groups = ast.literal_eval(os.environ.get('ALLOWED_GROUPS'))

    if not list(set(groups_list) & set(allowed_groups)):
        abort(403, description='you are not a member of an allowed group')


def get_groups(access_token):
    """
    returns a list which contains the groups of a user

    :param access_token: the access token by oidc provider
    :return: list containing user groups
    """
    headers = {"Authorization": "Bearer " + str(access_token)}
    response = requests.get(os.environ.get('GITLAB_GROUPS_URL'), headers=headers).json()

    groups = []
    for group in response:
        groups.append(group["name"])

    return groups


def get_user_information(access_token):
    """
    returns a dictionary with user's information
    currently, we include "id", "username","name" and "email" of a user

    :param access_token: the access token by oidc provider
    :return: dict containing "id", "username","name" and "email"
    """
    headers = {"Authorization": "Bearer " + str(access_token)}
    response = requests.get(os.environ.get('GITLAB_USERINFO_URL'), headers=headers).json()
    user_information = {
        "id": response["id"],
        "username": response["username"],
        "name": response["name"],
        "email": response["email"]
    }
    return user_information


def get_organization_group(access_token):
    """
    returns the user organization group starting but not equal with 'organization-group-', otherwise aborts with 403

    :param access_token: the access token by oidc provider
    :return: user's organization group
    """

    group_prefix = 'organization-group-'

    user = get_user_information(access_token)['username']

    groups = get_groups(access_token)

    groups_list = [i for i in groups if i.startswith(group_prefix) and i != group_prefix]

    if len(groups_list) > 1:
        logging.error(f'user {user} is member of multiple groups starting with "{group_prefix}"')
        abort(403, description=f'users that are members to multiple groups starting with "{group_prefix}" are not allowed')

    if len(groups_list) == 0:
        logging.error(f'user {user} is not a member of a group starting with with "{group_prefix}"')
        abort(403, description=f'you are not a member of a group starting with with "{group_prefix}"')

    return groups_list[0]


cookies = {}


@app.route('/index.php', methods=["POST"])
def proxy_async():
    access_token = oidc.get_access_token()
    if not access_token:
        return Response(status=401)
    else:
        return proxy('/index.php')

@app.route('/', defaults={'path': ''}, methods=["GET", "POST", "PUT", "DELETE"])
@app.route('/<path:path>', methods=["GET", "POST", "PUT", "DELETE"])
def proxy_sync(path):
  return proxy(path)

@oidc.require_login
def proxy(path):

    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return redirect(VAST_PUBLIC_URL)

    check_allowed_user(access_token)

    group = get_organization_group(access_token)
  

    url = f'vast-{group}:80'
    conn = http.client.HTTPConnection(url)
    headers = dict(request.headers)
    headers.pop('Host', None)
    path = request.environ['RAW_URI']
    try:
        if request.method == 'GET':
            conn.request(request.method, path, headers=headers)
        else:
            conn.request(request.method, path, request.get_data(), headers)
        resp = conn.getresponse()
    except Exception as e:
        return f'Cannot connect to {url}.Error: {str(e)}', 404

    headers=dict(resp.headers)
    headers.pop('Content-Length', None)
    headers.pop('Transfer-Encoding', None)
    content = resp.read()

    flask_response = Response(response=content, headers=headers, status=resp.status)
    return flask_response


if __name__ == '__main__':
    app.run()
