FROM python:3.10.2-alpine3.15

ENV GUNICORN_WORKERS=1
ENV GUNICORN_THREADS=50

EXPOSE 80

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY app.py ./

CMD gunicorn -b 0.0.0.0:80 app:app --workers=$GUNICORN_WORKERS --threads=$GUNICORN_THREADS --timeout 8000 --log-level debug
